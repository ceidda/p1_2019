const readlineSync = require('readline-sync');

function convertir(valor) {
    return Number(valor);
}

var entrada = 0;
var resultado = 0;

while (!isNaN(entrada)) {
    entrada = readlineSync.question('Ingrese el valor: ');

    if (!isNaN(entrada)) {
        resultado += convertir(entrada);
    } else if (entrada === 'r') {
        console.info('ingresaste una r');
    } else if (entrada === 'h') {
        console.info('ingresaste una h');
    } else {
        console.info('voy a salir porque no es un numero');
    }
}

console.info('El resultado es: ', resultado);