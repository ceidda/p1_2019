(function () {

    $(document).ready(() => {
        Utils.renderHandlebarTemplate(
            "#contacto-form-template",
            "#contacto-contenedor",
            null);

        $("#contacto-form").submit((evento) => {
            var inputs = $("#contacto-form input[type=\"text\"]");
            console.info(inputs);
            var nombre = $(inputs[0]).val();
            var telefono = $(inputs[1]).val();

            var inputsFile = $("#contacto-form input[type=\"file\"]");
            var reader = new FileReader();
            reader.readAsDataURL(inputsFile[0].files[0]);

            var archivoBase64 = null;
            reader.onload = function () {
                archivoBase64 = reader.result;
                var contacto = new Contacto(nombre, telefono, null, archivoBase64);
                var contactos = Agenda.cargarContactosDeLocalStorage();
                contactos.push(contacto);
                Agenda.guardarContactosEnLocalStorage(contactos);
            };
 
            return false;
        });

    });

}());