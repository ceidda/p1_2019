(function () {

    var contactos = Agenda.cargarContactosDeLocalStorage();

    var contacto = null;
    // esta funcion se engancha al evento ready del document.
    $(document).ready(() => {
        var condicion = $.urlParam("nombre");
        contacto = contactos.find(c => c.nombre === condicion);
        console.info(contacto);

        Utils.renderHandlebarTemplate(
            "#contacto-form-template",
            "#contacto-contenedor",
            {contacto: contacto});

        Utils.renderHandlebarTemplate(
            "#select-colores",
            "#select-colores-contenedor",
            {colores: COLORES, colorSeleccionado: contacto.color.valor});

        $('#select-colores-contenedor select').change((event) => {
            var value = $('#select-colores-contenedor select option:selected').val();
            console.info(value);
            $(".contacto-form_nombre").css("background-color", value);
        });

        $("#contacto-form").submit((evento) => {
            var inputs = $("#contacto-form input[type=\"text\"]");
            console.info(inputs);
            var nombre = $(inputs[0]).val();
            var telefono = $(inputs[1]).val();
            var colorNombre = $("#contacto-form select").val();
            var color = COLORES.find(c => c.valor === colorNombre);
            contacto.nombre = nombre;
            contacto.telefono = telefono;
            contacto.color = color;

            Agenda.guardarContactosEnLocalStorage(contactos);
            /*
            var index = contactos.indexOf(contacto);
            if (index > -1) {
                contactos.splice(index, 1);
            }
            */

            return false;
        });

        $("#contacto-editar").click(() => {
            $('.input-editable').attr('readonly', false);
        });

    });

    console.info($.urlParam("nombre"));
}());
