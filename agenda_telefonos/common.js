// funcion auxiliar que permite obtener valores de la url
// ejemplo: http:././localhost/algo?nombre=pepe
// var param = $.urlParam("nombre")
// console.info(param) --> pepe
$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
        .exec(window.location.search);

    return (results !== null) ? results[1] || 0 : false;
};

var Agenda = {
    cargarContactosDeLocalStorage: () => {
        var string = localStorage.getItem("contactos");
        var contactos = JSON.parse(string);
        return contactos;
    },
    guardarContactosEnLocalStorage: (contactos) => {
        var json = JSON.stringify(contactos);
        localStorage.setItem('contactos', json);
    }
};

var Utils = {
    renderHandlebarTemplate: (origen, destino, context) => {
        // consigue el source del template declarado en index.html
        var source = $(origen).html();
        // transforma el source a un template de handlebars
        var template = Handlebars.compile(source);
        // el objecto context lo usamos para enviar datos al template
        //var context = {contacto: contacto};
        // transformamos el template en html
        var html = template(context);
        // agregamos el html al contenedor
        $(destino).append(html);
    }
};

function Contacto(nombre, telefono, color, foto) {
    this.nombre = nombre;
    this.telefono = telefono;
    this.color = color;
    this.foto = foto;
};

Contacto.prototype.mostrar = function () {
    return this.nombre + " - " + this.telefono;
};

Contacto.prototype.toString = () => {
    return "esto iba a estar zaaaaaarpado, pero no.... :(";
};

function Color(nombre, valor, clazz) {
    this.nombre = nombre;
    this.valor = valor;
    this.clazz = clazz;
}

COLORES = [];
COLORES.push(new Color('Rojo', '#c00', 'rojo'));
COLORES.push(new Color('Amarillo', '#cc0', 'amarillo'));
COLORES.push(new Color('Verde','#9c0', 'verde'));
COLORES.push(new Color('Azul','#09c', 'azul'));
COLORES.push(new Color('Violeta','#c0c', 'violeta'));
COLORES.push(new Color('Blanco','#fff', 'blanco'));
COLORES.push(new Color('Negro','#000', 'negro'));

Handlebars.registerHelper('isEq', function (value1, value2, options) {
    if(value1 === value2) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});