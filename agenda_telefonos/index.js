(function () {

    if (localStorage.getItem("contactos") === null) {
        console.info("creando array de contactos");
        localStorage.setItem("contactos", "[]");
    }

    $(document).ready(() => {
        try {
            var datos = localStorage.getItem("contactos");
            var jsonArray = JSON.parse(datos);
            /*$.ajax({
                url: "http://localhost:8080/"
            }).then(function (data) {
                console.info(data);
                data.forEach((obj) => {
                    var contacto = new Contacto(obj.titulo, 123);
                    agregarContactoFilaATabla(contacto);
                });
            });
            */
            jsonArray.forEach((obj) => {
                var contacto = new Contacto(obj.nombre, obj.telefono, null, obj.foto);
                agregarContactoFilaATabla(contacto);
            });

            $('.btn-borrar').click((event) => {
                var padre = $(event.currentTarget).parent();
                var nombre = $('.__nombre', padre).html();
                alert(nombre);
            });
        } catch (error) {
            console.error(error);
        }
    });

    // function agregarContactoFilaATabla(contacto) {}
    var agregarContactoFilaATabla = (contacto) => {
        Utils.renderHandlebarTemplate(
            "#contacto-template",
            "#contactos-contenedor",
            { title: "Detalles", contacto: contacto }
        )
    };

    document.agregar = () => {
        var nombre = document.getElementById("txtNombre").value;
        var telefono = document.getElementById("numNumero").value;

        var contacto = new Agenda.Contacto(nombre, telefono);

        var datos = localStorage.getItem("contactos");
        var datosArray = JSON.parse(datos);
        datosArray.push(contacto);
        var json = JSON.stringify(datosArray);
        localStorage.setItem("contactos", json);

        document.getElementById("txtNombre").value = "";
        agregarContactoFilaATabla(contacto);
    };

}());
