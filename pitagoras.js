(function () {

    var pitagoras = function (a, b, c) {
        var h;
        var c1;
        var c2;

        if(a > b && a > c) {
            h = a;
            c1 = b;
            c2 = c;
        } else if (b > a && b > c) {
            h = b;
            c1 = a;
            c2 = c;
        } else {
            h = c;
            c1 = a;
            c2 = b;
        }

        return (Math.pow(h, 2) === Math.pow(c1, 2) + Math.pow(c2, 2));
    };

    console.log(`el triangulo es rectangulo:
     ${pitagoras(5, 12, 13)}`);
})();